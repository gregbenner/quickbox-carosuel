;(function ( $, window, document, undefined ) {

    // Create the defaults once
    var pluginName = "quickBoxCarousel",
        defaults = {
            propertyName: "value",
            speed: 1000,
            delayBetween: 3000,
            transition: "slide",
            pauseOnHover: true,
            preloader: true
        };

    // The actual plugin constructor
    function Plugin( element, options ) {
        this.element = element;
        this.options = $.extend( {}, defaults, options );
        this._defaults = defaults;
        this._name = pluginName;
        this.init();
    }

    Plugin.prototype = {

        init: function() {
            var elem = this.element,
                $elem = $(elem),
                listItems = $elem.find('li');

            this.cssSetup($elem, listItems);
            this.preloader($elem);
        },

        cssSetup: function($elem, listItems) {
            var listItems = $elem.find('li:first-child'),
                itemWidth = listItems.innerWidth();

                console.log(itemWidth);

            $elem.css({
                'width' : '999999px',
                'position' : 'absolute',
                'list-style' : 'none'
            });

            listItems.css({
                'border' : 'solid 1px red'
            });

            $elem.parent("div").css({
                "position" : "relative",
                "width" : itemWidth,
                "overflow" : 'hidden',
                'height' : '100%'
            });

        },

        preloader: function($elem) {
            var spinner = "data:image/gif;base64,R0lGODlhEAAQAPIAAP///wAAAMLCwkJCQgAAAGJiYoKCgpKSkiH/C05FVFNDQVBFMi4wAwEAAAAh/hpDcmVhdGVkIHdpdGggYWpheGxvYWQuaW5mbwAh+QQJCgAAACwAAAAAEAAQAAADMwi63P4wyklrE2MIOggZnAdOmGYJRbExwroUmcG2LmDEwnHQLVsYOd2mBzkYDAdKa+dIAAAh+QQJCgAAACwAAAAAEAAQAAADNAi63P5OjCEgG4QMu7DmikRxQlFUYDEZIGBMRVsaqHwctXXf7WEYB4Ag1xjihkMZsiUkKhIAIfkECQoAAAAsAAAAABAAEAAAAzYIujIjK8pByJDMlFYvBoVjHA70GU7xSUJhmKtwHPAKzLO9HMaoKwJZ7Rf8AYPDDzKpZBqfvwQAIfkECQoAAAAsAAAAABAAEAAAAzMIumIlK8oyhpHsnFZfhYumCYUhDAQxRIdhHBGqRoKw0R8DYlJd8z0fMDgsGo/IpHI5TAAAIfkECQoAAAAsAAAAABAAEAAAAzIIunInK0rnZBTwGPNMgQwmdsNgXGJUlIWEuR5oWUIpz8pAEAMe6TwfwyYsGo/IpFKSAAAh+QQJCgAAACwAAAAAEAAQAAADMwi6IMKQORfjdOe82p4wGccc4CEuQradylesojEMBgsUc2G7sDX3lQGBMLAJibufbSlKAAAh+QQJCgAAACwAAAAAEAAQAAADMgi63P7wCRHZnFVdmgHu2nFwlWCI3WGc3TSWhUFGxTAUkGCbtgENBMJAEJsxgMLWzpEAACH5BAkKAAAALAAAAAAQABAAAAMyCLrc/jDKSatlQtScKdceCAjDII7HcQ4EMTCpyrCuUBjCYRgHVtqlAiB1YhiCnlsRkAAAOwAAAAAAAAAAAA==";
            var images = $elem.find('img');

            images.css({
                'visibility' : 'hidden',
                'background' : '#CCC'
            });

            images.wrap('<div class="loading" />');
            $(".loading").css({
                'background' : '#CCC url(' + spinner + ') no-repeat center'
            });


            images.load(function() {
                images.css({
                    'visibility' : 'visible'
                });

                // fade in images and out spinner

                //$(this).unwrap();
            });
            // images each on load remove spinner and show image
        },

        animation: function() {
            var prevItem,
                thisItem,
                nextItem;
            var timeline = new TimelineLite();

            //timeline.fromTo(prevItem, 1.5, {width:0, height:0}, {width:100, height:200});
            //timeline.fromTo(thisItem, 1.5, {width:0, height:0}, {width:100, height:200});
        }
    };

    $.fn[pluginName] = function ( options ) {
        return this.each(function () {
            if (!$.data(this, "plugin_" + pluginName)) {
                $.data(this, "plugin_" + pluginName, new Plugin( this, options ));
            }
        });
    };

})( jQuery, window, document );